## LoopView

A simple circular scroll control, which requires only a few lines of code to achieve the circular scroll view..

## How to use

**You need to set up the itemClassName and the data source.**

    LoopView *loopView = [[LoopView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,
    self.view.bounds.size.height) itemClassName:@"TestView"];
    [loopView setUpViewsWithArray:dataArray];//dataSource: [model1,model2...]
    [loopView startAutoplay:1];//loop time interval
    [self.view addSubview:loopView];
    
## What the itemClass needs to set up

1. **First You must inherit the UIView.**
2. **@property a model and rewrite it`s set function,  loopView will auto set model from key-value coding.**

        @property(nonatomic,strong)TestModel*model;
        - (void)setModel:(TestModel *)model{

        _imageView.image = [UIImage imageNamed:model.imageName];
        }

